# Homer

Homer is a DoH (DNS-over-HTTPS) and DoT (DNS-over-TLS) client. Its
main purpose is to test DoH and DoT resolvers.

It is currently quite experimental.

## Usage

Two mandatory arguments, the URL of the DoH server (or name/address of
the DoT resolver), and a domain name to query. By default, Homer uses
DoH. Also by defaut, the type of data is AAAA (IP address). You can
add a third argument to use another type, as in the second example
below.

```
% homer https://doh.powerdns.org/ framagit.org
id 0
opcode QUERY
rcode NOERROR
flags QR RD RA
;QUESTION
framagit.org. IN AAAA
;ANSWER
framagit.org. 10800 IN AAAA 2a01:4f8:200:1302::42
;AUTHORITY
;ADDITIONAL
Total elapsed time: 0.40 seconds (402.28 ms/request)

% homer --dot 9.9.9.9 cocca.fr A
id 42545
opcode QUERY
rcode NOERROR
flags QR RD RA
;QUESTION
cocca.fr. IN A
;ANSWER
cocca.fr. 43200 IN A 185.17.236.69
;AUTHORITY
;ADDITIONAL

Total elapsed time: 0.07 seconds (66.72 ms/request )
```

Possible options, besides `--dot`:

* --verbose or -v: Makes the program more talkative
* --head or -e: (DoH) Uses only the HEAD HTTP method. Since the RFC
  does not mention it, result is probably indefinite.
* --POST or -P: (DoH) Uses the POST HTTP method (default is GET)
* --insecure or -k: Does not check the certificate

### Repetition of tests

You can repeat the tests several times, for instance for performance
measurements. This is done with option `--repeat N` where N is the
number of repetitions.

```
% homer --repeat 3 https://doh.bortzmeyer.fr ça.fr SOA
Test 0
...
Test 1
...
Test 2

Total elapsed time: 0.10 seconds (33.56 ms/request , 7.88 ms/request if we ignore the first one)
```

Homer reuses the same connection for all requests, both for DoH and
DoT, which explains why the first request is often longer.

Repetition is often combined with the use of an external file, where
Homer reads the domain names (and types) to query. Here is a sample
file:

```
truc.fr
chose.fr
machin.fr
trucmachin.fr NS
```

Assuming the file is named `list.txt`, this command will run four
tests, with the above names (and the query type `NS` for the last
one):

```
% homer --repeat 4 --file list.txt https://doh.42l.fr/dns-query
```

When repeating tests, you can add a delay between tests, with `--delay
N`, where N is the (possibly fractional) number of seconds to wait.

### Monitoring with Nagios, Icinga, or similar software

If the program is named `check_doh` or ` check_doh` (either from
copying or symbolic linking), it will behave as a [monitoring
plugin](https://www.monitoring-plugins.org/), suitable to be used from monitoring program like Nagios
or [Icinga](https://icinga.com/). The options are different in that case, and follow the
monitoring plugins conventions:

* -H: host name or address to monitor
* -V: virtual hostname (the certificate check will be based on that)
* -n: domain name to lookup
* -t: DNS type to query
* -p: (DoH) path in the URLx
* -P: uses the HTTP method POST
* -h: uses the HTTP method HEAD
* -i: insecure (do not check the certificate)

For Icinga, the following definition enables the plugin:

```
object CheckCommand "doh_monitor" {
  command = [ PluginContribDir + "/check_doh" ]

  arguments = {
      "-H" = "$address6$",
	  "-n" = "$doh_lookup$",
	  "-p" = "$doh_path$",
	  "-V" = "$doh_vhost$",
	  "-t" = "$doh_type$",
	  "-p" = "$doh_post$",
	  "-i" = "$doh_insecure$",
	  "-h" = "$doh_head$"	
	  }
}

object CheckCommand "dot_monitor" {
  command = [ PluginContribDir + "/check_dot" ]

  arguments = {
      "-H" = "$address6$",
	  "-n" = "$dot_lookup$",
	  "-p" = "$dot_path$",
	  "-V" = "$dot_vhost$",
	  "-t" = "$dot_type$",
	  "-p" = "$dot_post$",
	  "-i" = "$dot_insecure$",
	  "-h" = "$dot_head$"						     
	}
}

```

And a possible use is:

```
apply Service "doh" {
  import "generic-service"
  check_command = "doh_monitor"
    assign where (host.address || host.address6) && host.vars.doh
      vars.doh_lookup = "fr.wikipedia.org"

}

apply Service "dot" {
  import "generic-service"
  check_command = "dot_monitor"
    assign where (host.address || host.address6) && host.vars.dot
      vars.dot_lookup = "fr.wikipedia.org"

}

```

```
object Host "myserver" {
...
  vars.dot = true
  vars.dot_vhost = "dot.me.example"

  vars.doh = true
  vars.doh_vhost = "doh.me.example"
  vars.doh_post = true

```

## Installation

You need Python 3, [DNSpython](http://www.dnspython.org/), [PyOpenSSL](https://www.pyopenssl.org/) and
[pycurl](http://pycurl.io/docs/latest). You can install them with pip
`pip3 install dnspython pyOpenSSL pycurl`. Then, just run the script
`homer` (or `homer.py`).

On Debian, if you prefer regular operating system packages to pip,
`apt install python3 python3-dnspython python3-openssl python3-pycurl` will
install everything you need.

## Public servers

(Managed by non-profit organisations.)

### DoH

* `https://doh.powerdns.org/`
* `https://doh.bortzmeyer.fr/` ([Documentation](https://doh.bortzmeyer.fr/about)) 
* `https://doh.42l.fr/dns-query` 
* `https://odvr.nic.cz/doh` ([Documentation](https://www.nic.cz/odvr/))
* `https://dns.hostux.net/dns-query`
  ([Documentation](https://dns.hostux.net/))
* `https://ldn-fai.net/dns-query` ([Documentation in french](https://ldn-fai.net/serveur-dns-recursif-ouvert/))
* `https://dns.digitale-gesellschaft.ch/dns-query` ([Documentation in german](https://www.digitale-gesellschaft.ch/dns/))
* `https://doh.ffmuc.net` ([Documentation](https://ffmuc.net/wiki/doku.php?id=knb:dohdot_en))

### DoT

* `dot.bortzmeyer.fr` ([Documentation](https://doh.bortzmeyer.fr/about)) 
* `dns.digitale-gesellschaft.ch` ([Documentation in german](https://www.digitale-gesellschaft.ch/dns/))
* `dot.ffmuc.net` ([Documentation](https://ffmuc.net/wiki/doku.php?id=knb:dohdot_en)) 
* `ns0.ldn-fai.net` ([Documentation in french](https://ldn-fai.net/serveur-dns-recursif-ouvert/))

## License 

GPL. See LICENSE.

## Author

Stéphane Bortzmeyer <stephane+framagit@bortzmeyer.org>

## Reference site

https://framagit.org/bortzmeyer/homer/ Use the Gitlab issue tracker to
report bugs or wishes.
